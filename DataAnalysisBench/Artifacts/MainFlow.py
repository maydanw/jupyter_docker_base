
# coding: utf-8

# # Bio Workbench: Main Flow
# _Maydan Wienreb_
# <img src="./Resources/WebResources/hr-brian-litzinger-one.png" alt="Drawing" style="width: 100%; height: 60px" />

# ## Importing packages

# In[24]:


"""
This will enable completion on elements of lists, results of function calls, etc.
However, be warned that enabling this option can be unsafe, because the code is actually evaluated upon hitting TAB
"""
get_ipython().run_line_magic('config', 'IPCompleter.greedy=True')

# import numpy
import numpy as np
np.random.seed(0) # Remmeber to reset the seed every time if you expect the second draw to be the same as the first.

#import pandas
import pandas as pd
pd.set_option("display.max_rows", 20)
pd.set_option("display.max_columns", 500)
pd.set_option("display.width", 1000)


# In[25]:


# import graphics

get_ipython().run_line_magic('matplotlib', 'inline')
# %matplotlib notebook

import matplotlib.pyplot as plt
from matplotlib import rcParams
import seaborn as sns

from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))

# %load_ext autoreload
# %autoreload 1
# %aimport TestFunctions
## See: https://ipython.org/ipython-doc/3/config/extensions/autoreload.html


# In[26]:


rcParams['figure.figsize'] = [12.0, 8.0]
rcParams['pdf.fonttype'] = 42 ## Output Type 3 (Type3) or Type 42 (TrueType)
rcParams['font.sans-serif'] = 'Arial'
sns.set_style("whitegrid")


# In[27]:


import types
import sys
print("Basic packages versions:")
print("   Python version: %s" % (sys.version))
print("\n   Packages:")
print("  ","-"*10)
try:
    g = globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print("   %s: %s" % (mod.__name__, mod.__version__))
except:
    g = globals().values()
    for i, mod in enumerate(g):
        if isinstance(mod, types.ModuleType):
            if hasattr(mod, '__name__') and hasattr(mod, '__version__'):
                print("   %s: %s" % (mod.__name__, mod.__version__))


# ## Loading data
