#!/usr/bin/env bash

docker build -t jupyter_docker_base .

docker run -it -p 10000:8888 -p 6006:6006 \
  -v %CD%/DataBench:/workdir \
  -v %CD%/Resources:/workdir/Resources \
  jupyter_docker_base start-notebook.sh

